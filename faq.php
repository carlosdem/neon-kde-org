<?php
    include('templates/header.php');
?>

    <main class="KLayout">
        <section>
            <article id="what-is-neon">
                <h1 class="faqQuestion">FAQ</h1>
                <h4 class="faqQuestion">What is KDE neon? <a href="#what-is-neon">🔗</a></h4>
                <div>
                    <p>
                        KDE neon is a Linux distribution built on top of the latest
                        Ubuntu LTS release (22.04 at the moment) that showcases KDE
                        software exactly as the KDE developers intended it, with no
                        patches and no changes to default settings.  Adventurous
                        users are encouraged to try out User Edition.  KDE testers
                        can try out unreleased KDE software using the Testing and
                        Unstable Editions.
                    </p>
                </div>
            </article>

            <article id="is-this-the-distro">
                <h4 class="faqQuestion">Is this &quot;the KDE distro&quot;? <a href="#is-this-the-distro">🔗</a></h4>
                <div>
                    <p>
                        Nope. KDE believes it is important to work with many
                        distributions, as each brings unique value and
                        expertise for its users.  KDE neon is one distro out
                        of many, and that diversity is a good thing!
                    </p>
                </div>
            </article>

            <article id="who-is-it-for">
                <h4 class="faqQuestion">Who is KDE neon for? <a href="#who-is-it-for">🔗</a></h4>
                <div>
                    <p>
                        KDE Neon is primarily intended for technical Linux/KDE users who
                        want immediate access to the latest KDE offerings. The "Testing" and
                        "Unstable" editions are for users who are on a mission to see
                        KDE apps and Plasma Desktop succeed and are willing to contribute
                        to KDE by becoming Beta testers of the software. The "User" edition
                        is for enthusiast KDE users who expect a bit more polish. Please note
                        that the focus of the "User" edition is still KDE software *only*.
                        There is no thorough review of the complete software stack to guarantee
                        a rock solid day-to-day experience.</p>

                        <p>KDE developers endeavor to minimize bugs and maximize stability
                        within the scope of the KDE software stack. However, using the latest
                        software the moment it’s released will inevitably result in a less
                        stable experience compared to distros that delay software by days,
                        weeks, or months. If you have mission-critical reliability needs,
                        KDE Neon might not be the right distro for you.
                    </p>
                </div>
            </article>

            <article id="nvidia-support">
                <h4 class="faqQuestion">What kind of support for NVIDIA graphics cards does Neon offer? <a href="#nvidia-support">🔗</a></h4>
                <div>
                    <p>
                        KDE neon supports the open-source Nouveau driver only,
                        which should suffice for normal hardware-accelerated
                        desktop use. The proprietary driver is not supported at
                        all, and we recommend against trying to install it
                        anyway. It may work or it may not, and either way you’ll
                        be in "there be dragons" territory. If you find yourself
                        tempted to ignore this advice, then neon may not be the
                        right choice for you, and we instead recommend selecting
                        <a href="https://kde.org/distributions/">a different
                        distro.</a>
                    </p>
                </div>
            </article>

            <article id="non-lts-releases">
                <h4 class="faqQuestion">Will there be a version using non LTS Ubuntu releases? <a href="#non-lts-releases">🔗</a></h4>
                <div>
                    <p>No, we plan only to base on the latest LTS version of Ubuntu, this comes with Linux and graphics stack updates to
                    keep drivers relevant.  We will backport other software as needed.</p>
                </div>
            </article>

            <article id="is-it-rolling">
                <h4 class="faqQuestion">Is it a rolling distro? <a href="#is-it-rolling">🔗</a></h4>
                <div>
                    <p>
                        KDE neon is rolling for KDE software.  The Ubuntu base OS
                        is not, but certain packages will be updated as needed to
                        support KDE software requiring newer library versions than
                        what is provided by Ubuntu.

                        Apps from the main repositories are not rolling either,
                        and therefore can be up to two years old.  Users are
                        encouraged not to use them, and to instead get apps from
                        Snap or Flatpak using KDE’s Discover app store.  In
                        neon, Discover is set up to only show apps from these
                        sources, filtering out apps from the repositories.
                    </p>
                </div>
            </article>

            <article id="diff-ubuntu">
                <h4 class="faqQuestion">What is the difference between KDE neon and using plain Ubuntu? <a href="#diff-ubuntu">🔗</a></h4>
                <div>
                    <p>
                        KDE neon will provide users with more up-to-date
                        packages of Qt and cutting-edge KDE software.
                    </p>
                </div>
            </article>

            <article id="why-ubuntu">
                <h4 class="faqQuestion">Why Ubuntu? <a href="#why-ubuntu">🔗</a></h4>
                <div>
                    <p>
                        We use Ubuntu as a base for KDE neon because the KDE neon team feels it offers
                        the best technology as a stable release and the
                        best third party support. The KDE neon team is familiar
                        with Ubuntu having worked with it for over a decade.
                        We also feel that Ubuntu users will miss out if they
                        do not have up to date KDE software.  It is otherwise unrelated to the
                        Ubuntu project or Canonical.
                    </p>
                </div>
            </article>

            <article id="morph-kubuntu">
                <h4 class="faqQuestion">Can I turn Kubuntu into KDE neon with a PPA? <a href="#morph-kubuntu">🔗</a></h4>
                <div>
                    <p>
                        We recommend that you install a fresh KDE neon from the provided
                        ISO images.
                        But you can indeed add an APT repository to switch from Kubuntu
                        to KDE neon. This is absolutely not tested or supported. If things
                        take a turn for the worse you are expected to be knowledgable enough
                        to repair your system on your own. A web search should quickly give
                        you relevant information on how to do this.
                    </p>
                </div>
            </article>

            <article id="diff-kubuntu">
                <h4 class="faqQuestion">Is KDE neon an add-on to Kubuntu? <a href="#diff-kubuntu">🔗</a></h4>
                <div>
                    <p>
                        KDE neon sits on top of the Ubuntu core foundations, which means
                        the majority of software built for Ubuntu core will work fine,
                        even when not    explicitly supported by the KDE neon team. KDE neon is
                        however not    compatible with Kubuntu, as there is vast overlap in the
                        base offerings of both Kubuntu and KDE neon. You can not use both
                        systems at the same time. Installing KDE neon will simply replace
                        Kubuntu.
                    </p>
                </div>
            </article>

            <article id="diff-desktop">
                <h4 class="faqQuestion">Can I use a desktop other than Plasma? <a href="#diff-desktop">🔗</a></h4>
                <div>
                    <p>
                        This probably won’t work (probably won’t even install). It
                        most certainly isn’t supported even if the desktop installs. KDE
                        neon focuses on KDE software, most other software is not supported
                        and you should not be surprised if you can not install it or it
                        stops working at any point in time due to an update.
                    </p>
                </div>
            </article>

            <article id="64bit">
                <h4 class="faqQuestion">Is KDE neon 64bit only? <a href="#64bit">🔗</a></h4>
                <div>
                    <p>
                        Yes. As computers without 64bit have become increasingly rare,
                        we have chosen to focus our resources on higher-quality 64bit builds.
                    </p>
                </div>
            </article>

            <article id="ssl-mirrors">
                <h4 class="faqQuestion">Why are the download mirrors not https? <a href="#ssl-mirrors">🔗</a></h4>
                <div>
                    <p>
                        The mirrors are generously donated for KDE use by various organisations
                        but several don’t support https so our downloads are http only.  However
                        the images are GPG signed so you can be sure they match what was created by our builders.
                    </p>
                </div>
            </article>

            <article id="what-is-kde">
                <h4 class="faqQuestion">What is KDE? <a href="#what-is-kde">🔗</a></h4>
                <div>
                    <p>
                        The KDE® Community is an international technology team dedicated
                        to creating a free and user-friendly computing experience, offering
                        an advanced graphical desktop, a wide variety of applications for
                        communication, work, education and entertainment and a platform to
                        easily build new applications upon.
                    </p>
                </div>
            </article>

            <article id="command-to-update">
                <h4 class="faqQuestion">How do I Update to the Latest Software? <a href="#command-to-update">🔗</a></h4>
                <div>
                    <p>
                        KDE neon does continuous deployment of the latest KDE software which means there are nearly always new versions of our software to update to.  We recommend using Plasma Discover’s updater which appears in your panel: <img src="images/upgrade.png" width="28" height="27" />
                    </p>
                    <p>
                        If you prefer to use the command line you can use the pkcon command:
                        </p>
                        <ul>
                            <li><code>pkcon refresh</code></li>
                            <li><code>pkcon update</code></li>
                        </ul>
                     <p>This will install all new packages and uses the same PackageKit code as Plasma Discover.  Some uses of <code>apt</code> do not install new packages which makes it less suitable for KDE neon.</p>
                </div>
            </article>
        </section>
    </main>
<?php
    include ('templates/footer.php');
