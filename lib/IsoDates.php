<?php

class IsoDates {
    public $unstable;
    public $testing;
    public $user;
    public $developer;
    const DOWNLOAD_PERIOD = 60; // 1 minute
    
    
    function __construct() {
        // if json file is recent read that
        $this->datesFile = '/tmp/IsoDates.json';
    }

    // this init stuff is not in the construct() method so we can have isolated test cases
    function init() {
        if (!file_exists($this->datesFile) || time()-filemtime($this->datesFile) > self::DOWNLOAD_PERIOD) {
            $this->probeFilesKdeOrg();
            $fp = fopen($this->datesFile, 'w');
            fwrite($fp, json_encode(['unstable'=>$this->unstable, 'testing'=>$this->testing, 'user'=>$this->user, 'developer'=>$this->developer], JSON_PRETTY_PRINT));
            fwrite($fp, "\n");
            fclose($fp);
        } else {
            $json = file_get_contents($this->datesFile);
            $hash = json_decode($json, true);
            $this->unstable = $hash['unstable'];
            $this->testing = $hash['testing'];
            $this->user = $hash['user'];
            $this->developer = $hash['developer'];
            return true;
        }
    }

    function probeFilesKdeOrg() {
        $this->unstable = $this->probeIsoDir('unstable');
        $this->testing = $this->probeIsoDir('testing');
        $this->user = $this->probeIsoDir('user');
        $this->developer = $this->probeIsoDir('developer');
    }
    
    function probeIsoDir($edition) {
        $dirListingPage = file_get_contents('https://files.kde.org/neon/images/'.$edition.'/');
        $matched = preg_match('/20\d\d\d\d\d\d-\d\d\d\d/', $dirListingPage, $matches);
        if ($matched == 1) {
            return $matches[0];
        } else {
            return nil;
        }
    }    
}
